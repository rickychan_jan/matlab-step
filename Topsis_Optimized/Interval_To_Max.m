function [Positive_X]=Interval_To_Max(Column_Vector,Upper_bound,Lower_bound)
    M=max([Upper_bound-min(Column_Vector),max(Column_Vector)-Lower_bound]);
    r=size(Column_Vector,1);
    Positive_X=zeros(r,1);
    for i=1:r
        if Column_Vector(i)<Upper_bound
            Positive_X(i)=1-(Upper_bound-Column_Vector(i))/M;
        elseif Column_Vector(i)>Lower_bound
            Positive_X(i)=1-(Column_Vector(i)-Lower_bound)/M;
        else
            Positive_X=1;
        end
    end
end