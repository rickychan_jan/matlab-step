function [Positive_X]=Mid_To_Max(Column_Vector,Best_num)
    M=max(abs(Column_Vector-Best_num));
    Positive_X=1-abs(Column_Vector-Best_num)/M;
end