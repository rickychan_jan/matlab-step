function [fitresult, gof] = Population_Predict_Fitting(year, population)
%% Fit: 'Fitting_02'.
[xData, yData] = prepareCurveData( year, population );
% Set up fittype and options.
ft = fittype( 'xm/(1+(xm/3.9-1)*exp(-r*(t-1790)))', 'independent', 't', 'dependent', 'x' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.02 500];
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
% Plot fit with data.
figure( 'Name', 'Fitting_02' );
h = plot( fitresult, xData, yData );
legend( h, '人口样本数据', '拟合曲线', 'Location', 'NorthWest' );
% Label axes
xlabel('年份');
ylabel('人口');
grid on