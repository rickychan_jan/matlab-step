function [fitresult, gof] = Cftool_Function(x, y)
%% Fit: 'Fitting_01'.
[xData, yData] = prepareCurveData( x, y );
% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, 'Normalize', 'on' );
% Plot fit with data.
figure( 'Name', 'Fitting_01' );
h = plot( fitresult, xData, yData );
legend( h, '样本数据','拟合函数', 'Location', 'SouthEast' );
% Label axes
xlabel('x的值');
ylabel('y的值');
grid on
end