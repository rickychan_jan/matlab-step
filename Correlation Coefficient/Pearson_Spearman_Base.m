clear;clc
load 'Fitness Data.mat'
%% 统计描述
MIN = min(Data);
MAX = max(Data);
MEAN = mean(Data);
MEDIAN = median(Data);
SKEWNESS = skewness(Data);
KURTOSIS = kurtosis(Data);
STD = std(Data);
RESULT = [MIN;MAX;MEAN;MEDIAN;SKEWNESS;KURTOSIS;STD];
%% 皮尔逊相关系数
[R_p,P_p] = corrcoef(Test);
%% 假设检验部分
x = -4:0.1:4;
y = tpdf(x,28);
figure(1);
plot(x,y,'-');
grid on;
hold on;
tinv(0.975,28); %先将其求出，再画下面的图
plot([-2.048,-2.048],[0,tpdf(-2.048,28)],'r-');
plot([2.048,2.048],[0,tpdf(2.048,28)],'r-');
%% 计算p值
x = -4:0.1:4;
y = tpdf(x,28);
figure(2);
plot(x,y,'-');
grid on;
hold on;
plot([-3.055,-3.055],[0,tpdf(-3.055,28)],'r-');
plot([3.055,3.055],[0,tpdf(3.055,28)],'r-');
disp('该检验值对应的p值为：');
disp((1-tcdf(3.055,28))*2); %注意双侧检验的p值要乘以2
%% 正态分布检验 
n_colum = size(Data,2);
H = zeros(1,6);
P = zeros(1,6);
for i = 1:n_colum
    [h,p] = jbtest(Data(:,i),0.05);
    H(i)=h;
    P(i)=p;
end;
disp(H);
disp(P);
%% 斯皮尔曼相关系数
[R_s,P_s]=corr(Data, 'type' , 'Spearman');