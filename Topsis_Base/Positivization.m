function [Positive_X]=Positivization(Column_Vector,Vector_Type,Vector_Position)
    if Vector_Type==1
        disp(['··第',num2str(Vector_Position),'列是极小型，正在正向化··']);
        Positive_X=Min_To_Max(Column_Vector);
        disp(['·····第',num2str(Vector_Position),'列正向化完毕·····']);
        disp('============Dividing Line===========');
    elseif Vector_Type==2
        disp(['··第',num2str(Vector_Position),'列是中间型，正在正向化··']);
        Best_num=input('请输入最佳的数值:');
        Positive_X=Mid_To_Max(Column_Vector,Best_num);
        disp(['·····第',num2str(Vector_Position),'列正向化完毕·····']);
        disp('============Dividing Line===========');
    elseif Vector_Type==3
        disp(['··第',num2str(Vector_Position),'列是区间型，正在正向化··']);
        Upper_bound=input('请输入区间的上界:');
        Lower_bound=input('请输入区间的下届:');
        Positive_X=Interval_To_Max(Column_Vector,Upper_bound,Lower_bound);
        disp(['·····第',num2str(Vector_Position),'列正向化完毕·····']);
        disp('============Dividing Line===========');
    else
        disp('输入有误！');
    end
end