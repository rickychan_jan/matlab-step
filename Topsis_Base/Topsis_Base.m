clear;clc;
%cd 'D:\GiteeRepository\My_M_Code';
load data_of_water_quality.mat;
%正向化
[r,c]=size(X);
disp(['共有',num2str(r),'个评价对象,',num2str(c),'个评价指标']);
SELECT=input(['这',num2str(c),'个评价指标是否需要正向化，需要请输入yes，不需要请输入no:'],'s');

if strcmp(SELECT,'yes')
    Column_To_Deal=input('请输入需要正向化处理的评价指标所在列，例如:1、2、3列需要处理，则输入[1,2,3]:');
    disp('请输入需要处理的这些列的指标类型 1：极小型， 2：中间型， 3：区间型 ');
    Column_Type = input('例如：第1列是极小型，第2列是区间型，第3列是中间型，则输入[1,3,2]:');
    for i = 1:size(Column_To_Deal,2)
        X(:,Column_To_Deal(i))=Positivization(X(:,Column_To_Deal(i)),Column_Type(i),Column_To_Deal(i));
    end
    disp('正向化后的矩阵X=');
    disp(X);
else
    disp('不需要正向化');
end
%标准化
Z=X./repmat(sum((X.*X),1).^(1/2),r,1);
disp('标准化后的矩阵Z=');
disp(Z);
%计算得分并归一化
D_positive=sum((Z-repmat(max(Z),r,1)).^2,2).^(1/2);
D_negative=sum((Z-repmat(min(Z),r,1)).^2,2).^(1/2);
Score_tmp=D_negative/(D_positive+D_negative);
Score_final=Score_tmp/sum(Score_tmp);
disp('最终得分为:');
disp(Score_final);
[Score_sorted,index]=sort(Score_final,'descend');