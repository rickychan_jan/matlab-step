clear;clc;

subplot(2,2,1);
x=-pi:pi;
y=sin(x);
new_x=-pi:0.1:pi;
p=pchip(x,y,new_x);
plot(x,y,'kx',new_x,p,'r-');
title('分段三次埃尔米特插值');
legend('样本点','拟合曲线','Location','NorthWest');

subplot(2,2,2);
x=-pi:pi;
y=sin(x);
new_x=-pi:0.1:pi;
p=spline(x,y,new_x);
plot(x,y,'kx',new_x,p,'r-');
title('三次样条插值');
legend('样本点','拟合曲线','Location','NorthWest');

subplot(2,2,[3,4]);
x=-pi:pi;
y=sin(x);
new_x=-pi:0.1:pi;
p=interpn(x,y,new_x,'spline');
plot(x,y,'kx',new_x,p,'r-');
title('n维数据的样条插值');
legend('样本点','拟合曲线','Location','NorthWest');

