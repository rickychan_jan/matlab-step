clear;clc;
A=input('请输入判断矩阵A：');
ERROR=0;
[r,c]=size(A);

if r~=c
    ERROR=1;
end

if r<=1
    ERROR=2;
end

if r>15
    ERROR=3;
end

if sum(sum(A<=0))>0
    ERROR=4;
end

if sum(sum(A'.*A~=ones(r)))>0
    ERROR=5;
end

if ERROR==0
    clear;clc;
    A =[1 1 4 1/3 3;
     1 1 4 1/3 3;
     1/4 1/4 1 1/3 1/2;
     3 3 3 1 3;
     1/3 1/3 2 1/3 1];
     %算数平均法
    A_sum=sum(A,1);%求列和
    n_col=size(A_sum,2);%求列数
    A_rep=repmat(A_sum,n_col,1);%转化成同型矩阵
    A_sta=A./A_rep;%按列归一化
    A_wei=sum(A_sta,2)/n_col;%求特征向量
    disp('算术平均法求得的权重为:');
    disp(A_wei);
    
    %几何平均法
    A_pro=prod(A,2);%行相乘得一个列向量
    A_pre=A_pro.^(1/n_col);%得到的列向量开n次方
    A_nor=A_pre/sum(A_pre);%列向量归一化
    disp('几何平均法求得的权重为:');
    disp(A_nor);
    
    %特征值法
    [V,D]=eig(A);%求特征向量与特征值
    D_max=max(max(D));%求最大的特征值
    [r,c]=find(D==D_max,1);%找出最大特征值所在的列数
    A_nor=V(:,c)/sum(V(:,c));%按列归一化
    disp('特征值法求得的权重为:');
    disp(A_nor);
    
    %计算一致性比例
    CI=(D_max-n_col)/(n_col-1);%计算一致性指标CI
    RI=[0,0,0.52,0.89,1.12,1.26,1.36,1.41,1.46,1.49,1.52,1.54,1.56,1.58,1.59];%查找对应的平均随机一致性指标
    CR=CI/RI(n_col);%计算一致性比例
    if CR<0.1
        disp('一致性可以接受');
    else
        disp('判断矩阵需要进行修改');
    end
elseif ERROR==1
    disp('请检查输入的判断矩阵行列数是否一致')
elseif ERROR==2
    disp('请检查输入的判断矩阵维度是否大于1');
elseif ERROR==3
    disp('请检查输入的判断矩阵维度是否小于等于15');
elseif ERROR==4
    disp('请检查输入的判断矩阵是否有小于等于0的元素');
elseif ERROR==5
    disp('请检查输入的判断矩阵是否为正互反矩阵');
end