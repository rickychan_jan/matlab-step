times = 300; %蒙特卡洛的次数
correlation_coefficient = zeros(times,1); %存扰动项u和x1的相关系数
regression_coefficient = zeros(times,1); %存遗漏了x2之后，只用y对x1回归得到的回归系数
for i = 1:times
    n = 30; %样本的数据量
    x1 = -10 + rand(n,1) * 20;
    random_number = normrnd(0,5,n,1) - rand(n,1);
    x2 = 0.3 * x1 + random_number; %这样得到的x1和x2的相关性不确定 0.3也是随便给的
    u = normrnd(0,1,n,1); %扰动项u服从标准正态分布
    y = 0.5 + 2 * x1 + 5 * x2 + u; %构造一个y
    k = (n*sum(x1.*y)-sum(x1)*sum(y))/(n*sum(x1.*x1)-sum(x1)*sum(x1)); %y = kx1 + b 回归估计出来k
    regression_coefficient(i) = k;
    u = 5 * x2 + u; %回归中忽略了5*x2，所以扰动项要加上5*x2
    r = corrcoef(x1,u); %求x1与u之间的相关系数
    correlation_coefficient(i) = r(2,1);
end
plot(correlation_coefficient,regression_coefficient,'b*');
xlabel('x1和u的相关系数');
ylabel('k的估计值');